# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql.conditionals import Coalesce
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval

__all__ = ['Production']

__metaclass__ = PoolMeta


class Production:
    __name__ = 'production'

    quantity_default_uom = fields.Function(fields.Float('Quantity in default UOM',
                                                        digits=(16, Eval('quantity_default_uom_digits', 2)),
                                                        depends=['quantity_default_uom_digits', 'quantity', 'uom']),
                                           'on_change_with_quantity_default_uom',
                                           searcher='search_quantity_default_uom')

    quantity_default_uom_digits = fields.Function(fields.Integer('Unit Digits'),
                                                  'on_change_with_quantity_default_uom_digits')

    @fields.depends('quantity', 'uom', 'product')
    def on_change_with_quantity_default_uom(self, name=None):
        pool = Pool()
        Uom = pool.get('product.uom')
        return Uom.compute_qty(self.uom, self.quantity, self.product.default_uom)

    @fields.depends('product')
    def on_change_with_quantity_default_uom_digits(self, name=None):
        if self.product:
            return self.product.default_uom.digits
        return 2

    def on_change_product(self):
        res = super(Production, self).on_change_product()
        res['quantity_default_uom_digits'] = self.on_change_with_quantity_default_uom_digits()
        return res

    @classmethod
    def search_quantity_default_uom(cls, name, clause):
        pool = Pool()
        Uom = pool.get('product.uom')
        Template = pool.get('product.template')
        Product = pool.get('product.product')
        _, operator, value = clause
        Operator = fields.SQL_OPERATORS[operator]
        table = cls.__table__()
        uom1 = Uom.__table__()
        uom2 = Uom.__table__()
        template = Template.__table__()
        product = Product.__table__()

        value = cls.quantity.sql_format(value)
        query = table.join(uom1, condition=table.uom == uom1.id
                    ).join(product, condition=table.product == product.id
                    ).join(template, condition=product.template == template.id
                    ).join(uom2, condition=template.default_uom == uom2.id
                    ).select(table.id,
                             where=Operator(Coalesce(table.quantity, 0) * uom1.factor * uom2.rate, value),
                             group_by=table.id)

        return [('id', 'in', query)]

    def explode_bom(self):
        pool = Pool()
        Uom = pool.get('product.uom')

        changes = super(Production, self).explode_bom()

        if not changes:
            return changes

        if changes.get('outputs'):
            for key, out_ in changes['outputs']['add']:
                bom_outputs = [i for i in self.bom.outputs
                               if i.product.id == out_['product']
                               and self.product.id == out_['product']
                               and i.uom.id == out_['uom']
                               and i.uom.id != self.uom.id]
                if bom_outputs:
                    out_['quantity'] = Uom.compute_qty(Uom(out_['uom']), out_['quantity'], self.uom)
                    out_['unit_price'] = Uom.compute_price(Uom(out_['uom']), out_['unit_price'], self.uom)
                    out_['uom'] = self.uom.id
                    out_['uom.rec_name'] = self.uom.rec_name

        return changes